# Additional Base Lib 附加基础库

用轻量级容器工具bubblewrap解决GNU/Linux操作系统中常见的glibc不兼容问题。

#### 概述

如果经常在GNU/Linux平台使用非系统软件源内的应用，很容易遇到这样的问题：  
``/lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.xx' not found (required by /path/to/xxx）``  
这是因为程序编译时的使用的glibc高于运行时使用的版本。网上流传的解决方案通常是让你升级系统中的glibc，但是由于glibc是GNU/Linux系统中极其重要的组件，贸然行事很容易造成系统损坏。尽管确实有一些更合理的解决方法，但是他们都比较麻烦。而这个小工具简单快捷，方便安装，没有任何安全隐患。

#### 许可

`ablrun`等脚本文件没有许可证限制。附带的glibc等动态库文件都是取自一些GNU/Linux发行版的，请遵守相应的许可。

#### 注意事项

1. 软件包可以安装在使用dpkg或rpm的发行版上。移植到其他发行版平台也很容易。
2. 大多数应用都能运行，但是一些需要权限的不可以。如果你发现了应该可以但实际上不能运行的应用，请向我报告。
3. 在glibc之外还打包了一个libstdc++的动态库，因为这个问题也比较常见。
4. 这个脚本只关注解决glibc的兼容问题，因此很多时候你还需要解决一些其他的动态库问题才能让应用运行。你可以在使用ablrun的同时使用`LD_LIBRARY_PATH`环境变量改变动态库寻找的位置，解决一些其他动态库问题。

#### 用法

首先需要安装additional-base-lib的软件包，可以在右侧发行版处下载。  
如果你使用的是debian衍生版，使用这个命令安装：  
`sudo apt install "path/to/package_name.deb"`  
基于rpm的发行版，请使用相应的包管理命令。

此后只需要在出现glibc问题的命令前面，加上ablrun和空格即可：  
`ablrun [命令 [运行选项 ...]]`

卸载也需要通过系统的包管理器。  
如果使用的是debian衍生版，使用这个命令卸载：  
`sudo apt remove additional-base-lib`  
基于rpm的发行版，请使用相应的包管理命令。

#### 其他资源

最初的发布页面：  
https://bbs.deepin.org/post/256555

解决动态库问题的通用解决方法：  
https://bbs.deepin.org/post/256081

#### 定制
如果这里发布的deb包不符合你的要求（库版本、架构），你可以使用`make-deb.sh`创造你自己的附加基础库。你需要将项目下载到本地，然后编辑这个脚本，把三个包的下载链接改成你想要的即可。你可以在debian的网站找到各种版本和架构的下载链接：https://www.debian.org/distrib/packages ，然后在当前目录中执行`make-deb.sh`即可。他会自动识别软件包的架构并采取相应方案。  
在这之前，你需要安装以下软件包：bash, coreutils, dpkg, dpkg-dev, grep, wget, patchelf

对于rpm包，你可以使用`make-rpm.sh`，你需要将项目下载到本地，然后编辑这个脚本，把三个包的下载链接改成你想要的即可。你可以在fedora的网站找到各种版本和架构的下载链接：https://packages.fedoraproject.org/ ，然后在当前目录中执行`make-rpm.sh`即可。他会自动识别软件包的架构并采取相应方案。rpm包会存储在`~/rpmbuild/RPMS`中。  
在这之前，你需要安装以下软件包：bash, coreutils, rpm-build, wget, patchelf
