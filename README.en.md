# Additional Base Lib

Solve the common compatible problem of glibc on GNU/Linux, by lightweight container utility bubblewrap.

#### Introdution

When running applications, which are not include in system repository, there's sometimes a problem like this, which prevent application to run:  
``/lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.xx' not found (required by /path/to/xxx）``  
That's because the application build with higher glibc then running. Someone choose to manually upgrade glibc, but because of the importance of glibc in GNU/Linux, this method usually make the whole OS distroyed. There's some safer but more complex solution, however, the utility here is much easier using and installing, without troubles to damage your operating system.

#### License

There's no license restriction with scripts such as `ablrun`. All the library files are taken from some GNU/Linux distributions, their original license should followed.

#### Notes

1. You can install it onto many distributions with dpkg or rpm package management system. And It's easy to port to other GNU/Linux platform.
2. Most applications can run with ablrun, but some needs privileges can't. If you found an application which should run but actually don't, please report.
3. Also include a single libstdc++ library with glibc, because this problem is as often as that with glibc. 
4. The script can only solve glibc campatible problem, not all the problems to make your application run. You may use `LD_LIBRARY_PATH` environment variable with ablrun to search libraries in other directories, which may solve some other library problem.

#### Usage

Install additional-base-lib package first. You may download from Releases section on the right side of the page.  
If you use debian-based GNU/Linux distribution, install with this command:  
`sudo apt install "path/to/package_name.deb"`  
If your distribution base on rpm, use package manage command provided by your distribution.

Then you can run command which invokes glibc problem, led by ablrun and a space:  
`ablrun [command [arguments ...]]`

When remove, You should also use package management.   
If you use debian-based GNU/Linux distribution, install with this command:  
`sudo apt remove additional-base-lib`  
If your distribution base on rpm, use package manage command provided by your distribution.

#### Resources

Original release page (in Chinese):  
https://bbs.deepin.org/post/256555

Common solution for many library problem (in Chinese):  
https://bbs.deepin.org/post/256081

#### Customize
If the released package cannot fit your needs (such as library version, architecture), for deb package, you may try `make-deb.sh` to create your own additional base lib. Download the project, and edit the script, change 3 package download URLs to which you want. You may found many version and architectures from debian page: https://www.debian.org/distrib/packages , and then run the script `make-deb.sh`. It will auto-detect architecture of packages, and take corresponding methods.  
Install these packages first: bash, coreutils, dpkg, dpkg-dev, grep, wget, patchelf

For rpm package, you need `make-rpm.sh`. Download the project, and edit the script, change 3 package download URLs to which you want. You may found many versions and architectures from fedora page: https://packages.fedoraproject.org/ , and then run the script `make-rpm.sh`. It will auto-detect architecture of packages, and take corresponding methods. Rpm package will store to `~/rpmbuild/RPMS` directory.  
Install these packages first: bash, coreutils, rpm-build, wget, patchelf
